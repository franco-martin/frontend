function redeploy(){
    backend = "https://api.francomartin.site/redeploy"
    fetch(backend)
    .then(response => response.json())
    .then(body => {
            let pipeline_id
            if(body.status == 'ok'){
                document.getElementById("replace-id-txt").innerHTML="Since you redeployed the site, you will need to refresh the webpage when the pipeline finishes. Click on See latest frontend pipeline to view the progress."
                document.getElementById("replace-id-link").href="https://gitlab.com/franco-martin/frontend/-/pipelines/"+body.pipeline.id
            }
            else{
                pipeline_id = 0
                alert(body.message)
            }
        }
    )
}