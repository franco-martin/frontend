async function getRss(){
    //rssFeed = "https://franco-martin.medium.com/feed"

    var req = await fetch('https://api.rss2json.com/v1/api.json?rss_url=https://franco-martin.medium.com/feed');
    var data = await req.json();
    if(data.status == "ok"){
        let numberOfPostsToPublish = 5;
        for (let i = 0; i < numberOfPostsToPublish; i++) {
            let article = data.items[i]
            console.debug(`Loading ${article.title}`)
            document.getElementById("replace-articles").innerHTML+=`
                            <!-- Article Content-->
                            <h3 class="masthead-subheading text-center text-uppercase text-secondary">${article.title}</h3>
                            <div class="column">
                                <div class="text-center row-lg-4 ml-auto"><p class="lead">${article.content.split("\n")[1]}</p></div>
                                <div class="text-center">
                                    <a href="${article.link}" class="button-center btn btn-secondary">Read the full article</a>
                                </div>
                            </div>
                            &nbsp;
            `
          }

    }
}