function startCanary(){
    backend = "https://api.francomartin.site/canary"
    fetch(backend)
    .then(response => response.json())
    .then(body => {
            let pipeline_id
            if(body.status == 'ok'){
                document.getElementById("watch-pipeline").href="https://gitlab.com/franco-martin/demo-canary/-/pipelines/"+body.pipeline.id
                document.getElementById("watch-pipeline").classList.remove("disabled")
                testCanary()
            }
            else{
                pipeline_id = 0
                alert(body.message)
            }
        }
    )
   
    //document.getElementById("start-deployment").innerHTML+="<a href=\"https://gitlab.com/franco-martin/demo-canary/-/pipelines/123\" class=\"button-center btn btn-secondary\">Watch the pipeline doing its thing</a>"

}

function testCanary(){
    var i
    for (i = 0; i < 25; i++) {
        backend = "https://demo-canary.francomartin.site"
        sleep(10000*i).then(() =>
        {
            fetch(backend)
            .then(response => response.text())
            .then(body => {
                    document.getElementById("responses").innerHTML+="<p>Server returned: "+body+"</p>"
                }
        )
        });
    }
}

function runTest(){
    backend = "https://demo-canary.francomartin.site"
    var i;
    for (i = 0; i < 5; i++) {
        fetch(backend)
        .then(response => response.text())
        .then(body => {
                document.getElementById("testResults").innerHTML+="<p>Server returned: "+body+"</p>"
            }
        )
    }
}

function cleanup(){
    document.getElementById("responses").innerHTML=""
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }